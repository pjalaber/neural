#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <getopt.h>
#include <sys/time.h>
#include "ffnn.h"

static const char ascii_grey[] =
	"$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\" ^`'. ";

typedef struct
{
	uint8_t digit;
#define MNIST_WIDTH       28
#define MNIST_HEIGHT      28
#define MNIST_PIXEL_COUNT (MNIST_WIDTH * MNIST_HEIGHT)
	double  pixel[28 * 28];
} mnist_digit_t;

static int mnist_csv_parse(mnist_digit_t *mnd, const char *csv)
{
	const char *ptr;
	uint32_t pixel = 0;
	uint8_t pixel_val;

	ptr = strchr(csv, ',');
	if (ptr == NULL)
		return -1;

	mnd->digit = atoi(csv);
	if ( mnd->digit > 9)
		return -1;
	for (; ptr != NULL && *ptr != '\0'; )
	{
		pixel_val = atoi(ptr + 1);
		mnd->pixel[pixel++] = (pixel_val / 255.0 * 0.99) + 0.01;
		if (pixel > MNIST_PIXEL_COUNT)
			return -1;
		ptr = strchr(ptr + 1, ',');
	} while (ptr != NULL);

	if (pixel != MNIST_PIXEL_COUNT)
		return -1;

	return 0;
}

static void mnist_display_digit(const mnist_digit_t *mnd, FILE *f)
{
	int32_t x, y;
	uint32_t index;

	for (y = 0; y < MNIST_HEIGHT; y++)
	{
		for (x = 0; x < MNIST_WIDTH; x++)
		{
			index = (1.0 - mnd->pixel[y * MNIST_HEIGHT + x]) * (sizeof(ascii_grey) - 1);
			fprintf(f, "%c", ascii_grey[index]);
		}
		fprintf(f, "\n");
	}
}

static FILE *file_open(const char *filename, const char *mode, uint32_t *lines_count)
{
	FILE *f;

	*lines_count = 0;

	f = fopen(filename, mode);
	if (f == NULL)
		return NULL;

	while (!feof(f)) {
		if (fgetc(f) == '\n')
			(*lines_count)++;
	}

	rewind(f);

	return f;
}

static void usage(void)
{
	fprintf(stderr,
	        "Usage: mnist  -t <mnist-csv-training-filename>\n"
	        "              -q <mnist-csv-query-filename>\n"
	        "             [-o mistmatch filename]\n");
}

typedef struct
{
	int32_t percent;
	uint32_t max;
	uint32_t align;
} progress_t;

static void progress_init(progress_t *p, uint32_t max)
{
	p->max     = max;
	p->align   = 0;
	while (max > 0)
	{
		p->align++;
		max = max / 10;
	}
}

static void progress_print(progress_t *p, const char *header, uint32_t current)
{
#define PROGRESS_SIZE 25
	uint32_t hashes = current * 1.0 * PROGRESS_SIZE / p->max;
	uint32_t spaces = PROGRESS_SIZE - hashes;

	printf("\r%s [", header);

	while (hashes--)
		putchar('=');

	while (spaces--)
		putchar(' ');

	printf("] %*u/%u", p->align, current, p->max);

	fflush(stdout);
}

typedef struct
{
	struct timeval tv1;
	struct timeval tv2;
} time_elapsed_t;

static void time_elapsed_start(time_elapsed_t *t)
{
	gettimeofday(&t->tv1, NULL);
}

static void time_elapsed_end(time_elapsed_t *t)
{
	gettimeofday(&t->tv2, NULL);
}

static void time_elapsed_print(const time_elapsed_t *t, const char *header)
{
	struct timeval tv_diff;

	timersub(&t->tv2, &t->tv1, &tv_diff);
	printf("%s%lu.%lu sec\n", header, tv_diff.tv_sec, tv_diff.tv_usec / 100000);
}

int main(int argc, char **argv)
{
	const char *train_filename    = NULL;
	const char *query_filename    = NULL;
	const char *mismatch_filename = NULL;
	FILE       *f_input           = NULL;
	FILE       *f_output          = NULL;
	int ch;
	ffnn_t *ffnn;
	mnist_digit_t mnd;
	double targets[10], max;
	const double *outputs;
	int32_t ret = -1;
	uint32_t i, line, line_count, max_i;
	uint32_t test_count, match_count;
	char csv_line[4096];
	progress_t progress;
	time_elapsed_t time_elapsed;

	while ((ch = getopt(argc, argv, "h:t:q:o:")) != -1) {
		switch (ch) {
		case 't':
			train_filename = optarg;
			break;
		case 'q':
			query_filename = optarg;
			break;
		case 'o':
			mismatch_filename = optarg;
			break;
		case 'h':
		default:
			usage();
			goto err;
		}
	}

	if (train_filename == NULL || query_filename == NULL)
	{
		usage();
		goto err;
	}

	ret = ffnn_autotest();
	if (ret != FFNN_OK)
	{
		fprintf(stderr, "ffnn_autotest error: %s\n", ffnn_err2str(ret));
		goto err;
	}

	ret = ffnn_new(&ffnn, MNIST_PIXEL_COUNT, 200, 10, 0.2);
	if (ret != FFNN_OK)
	{
		fprintf(stderr, "ffnn_init error: %s\n", ffnn_err2str(ret));
		goto err;
	}

	ffnn_random_weights(ffnn);

	f_input = file_open(train_filename, "r", &line_count);
	if (f_input == NULL)
	{
		fprintf(stderr, "Error opening %s file\n", train_filename);
		goto err;
	}

	for (i = 0; i < 10; i++)
		targets[i] = 0.01;

	line = 0;
	progress_init(&progress, line_count);
	time_elapsed_start(&time_elapsed);
	while (!feof(f_input))
	{
		if (fgets(csv_line, sizeof(csv_line), f_input) == NULL && !feof(f_input)) {
			fprintf(stderr, "Error reading line in %s\n", train_filename);
			goto err;
		}
		if (mnist_csv_parse(&mnd, csv_line) < 0)
		{
			printf("mnist_csv_parse error\n");
			goto err;
		}

		progress_print(&progress, "Training ffnn:", line);

		targets[mnd.digit] = 0.99;
		ffnn_train(ffnn, mnd.pixel, targets);
		targets[mnd.digit] = 0.01;

		line++;
	}

	time_elapsed_end(&time_elapsed);
	time_elapsed_print(&time_elapsed, " - ");

	fclose(f_input);

	f_input = file_open(query_filename, "r", &line_count);
	if (f_input == NULL)
	{
		fprintf(stderr, "Error opening %s file\n", query_filename);
		goto err;
	}

	if (mismatch_filename != NULL)
	{
		f_output = fopen(mismatch_filename, "w");
		if (f_output == NULL)
		{
			fprintf(stderr, "Error opening %s file\n", mismatch_filename);
			goto err;
		}
	}

	line = 0;
	test_count = 0;
	match_count = 0;
	progress_init(&progress, line_count);
	time_elapsed_start(&time_elapsed);
	while (!feof(f_input))
	{
		if (fgets(csv_line, sizeof(csv_line), f_input) == NULL && !feof(f_input))
		{
			fprintf(stderr, "Error reading line in %s\n", query_filename);
			goto err;
		}
		if (mnist_csv_parse(&mnd, csv_line) < 0)
		{
			printf("mnist_csv_parse error\n");
			goto err;
		}

		progress_print(&progress, "Querying ffnn:", line);
		outputs = ffnn_query(ffnn, mnd.pixel);
		max = 0.0;
		max_i = 0;
		for (i = 0; i < 10; i++)
		{
			if (outputs[i] >= max)
			{
				max = outputs[i];
				max_i = i;
			}
		}
		if (mnd.digit == max_i)
			match_count++;
		else if (f_output != NULL)
		{
			fprintf(f_output, "'%u' misrecognized as '%u'\n", mnd.digit, max_i);
			fprintf(f_output, "vvvv\n");
			mnist_display_digit(&mnd, f_output);
			fprintf(f_output, "\n");
		}

		line++;
		test_count++;
	}

	time_elapsed_end(&time_elapsed);
	time_elapsed_print(&time_elapsed, " - ");

	printf("Accuracy ffnn: %0.3g\n", (match_count * 1.0) / test_count);

	ret = 0;

err:
	ffnn_free(ffnn);
	if (f_input != NULL)
		fclose(f_input);
	if (f_output != NULL)
		fclose(f_output);
	return ret;
}
