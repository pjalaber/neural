#ifndef _FFNN_
#define _FFNN_

/* Feed forward neural network API (ffnn)
 */

typedef struct ffnn ffnn_t;

#define FFNN_OK     0
#define FFNN_EINVAL -1
#define FFNN_EMEM   -2

int32_t ffnn_new(ffnn_t **ffnn,
                 uint32_t in_nodes_count,
                 uint32_t hidden_nodes_count,
                 uint32_t output_nodes_count,
                 double learning_rate);

void ffnn_free(ffnn_t *ffnn);

const char *ffnn_err2str(int32_t ffnn_err);

void ffnn_random_weights(ffnn_t *ffnn);

void ffnn_train(ffnn_t *ffnn, const double *in_signal, const double *target_signal);

const double *ffnn_query(ffnn_t *ffnn, const double *in_signal);

int ffnn_autotest(void);

#endif
