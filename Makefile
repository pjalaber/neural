OBJECTS=mnist.o ffnn.o
EXE=mnist

CFLAGS+=-O3 -Werror -Wall
LDFLAGS+=-lm

$(EXE): $(OBJECTS)
	$(CC) -o $(EXE) $(OBJECTS) $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f $(OBJECTS) $(EXE)
