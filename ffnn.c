#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include "ffnn.h"

typedef struct ffnn
{
	double    learning_rate;
	uint32_t  nodes_count[3];
	double   *weights[2];
	double   *nodes[3];
	double   *hidden_errors;
	double   *output_errors;
} ffnn_t;

#define FFNN_WEIGHT(n, layer, node_src, node_dst) \
	((n)->weights[layer][(n)->nodes_count[(layer) + 1] * (node_src) + (node_dst)])

#define FFNN_NODE(n, layer, node)	  \
	((n)->nodes[layer][node])

#define FFNN_OK     0
#define FFNN_EINVAL -1
#define FFNN_EMEM   -2

#define FFNN_INPUT 0
#define FFNN_HIDDEN 1
#define FFNN_OUTPUT 2

const char *ffnn_err2str(int32_t ffnn_err)
{
	switch (ffnn_err)
	{
	case FFNN_OK:
		return "success";
	case FFNN_EINVAL:
		return "invalid value";
	case FFNN_EMEM:
		return "memory allocation error";
	default:
		return "unknown error";
	}
}

static double gaussrand(double mean, double deviation)
{
	static double V1, V2, S;
	static int phase = 0;
	double X;

	if( phase == 0)
	{
		do
		{
			double U1 = (double)rand() / RAND_MAX;
			double U2 = (double)rand() / RAND_MAX;

			V1 = 2 * U1 - 1;
			V2 = 2 * U2 - 1;
			S = V1 * V1 + V2 * V2;
		} while (S >= 1 || S == 0);

		X = V1 * sqrt(-2 * log(S) / S);
	} else
		X = V2 * sqrt(-2 * log(S) / S);

	phase = 1 - phase;

	return X * deviation + mean;
}

int ffnn_new(ffnn_t **ffnn,
             uint32_t in_nodes_count,
             uint32_t hidden_nodes_count,
             uint32_t output_nodes_count,
             double learning_rate)
{
	if (in_nodes_count == 0 || hidden_nodes_count == 0 || output_nodes_count == 0)
		return FFNN_EINVAL;

	*ffnn = calloc(sizeof(**ffnn), 1);
	if (ffnn == NULL)
		goto emem;

	(*ffnn)->nodes_count[FFNN_INPUT]  = in_nodes_count;
	(*ffnn)->nodes_count[FFNN_HIDDEN] = hidden_nodes_count;
	(*ffnn)->nodes_count[FFNN_OUTPUT] = output_nodes_count;
	(*ffnn)->learning_rate = learning_rate;

	(*ffnn)->nodes[FFNN_HIDDEN] = calloc(hidden_nodes_count, sizeof(double));
	if ((*ffnn)->nodes[FFNN_HIDDEN] == NULL)
		goto emem;
	(*ffnn)->nodes[FFNN_OUTPUT] = calloc(output_nodes_count, sizeof(double));
	if ((*ffnn)->nodes[FFNN_OUTPUT] == NULL)
		goto emem;

	(*ffnn)->weights[FFNN_INPUT] = calloc(in_nodes_count * hidden_nodes_count, sizeof(double));
	if ((*ffnn)->weights[FFNN_INPUT] == NULL)
		goto emem;
	(*ffnn)->weights[FFNN_HIDDEN] = calloc(hidden_nodes_count * output_nodes_count, sizeof(double));
	if ((*ffnn)->weights[FFNN_HIDDEN] == NULL)
		goto emem;

	(*ffnn)->output_errors = calloc(output_nodes_count, sizeof(double));
	if ((*ffnn)->output_errors == NULL)
		goto emem;

	(*ffnn)->hidden_errors = calloc(hidden_nodes_count, sizeof(double));
	if ((*ffnn)->hidden_errors == NULL)
		goto emem;

	return FFNN_OK;

emem:
	ffnn_free(*ffnn);
	return FFNN_EMEM;
}

void ffnn_free(ffnn_t *ffnn)
{
	if (ffnn == NULL)
		return;

	free(ffnn->weights[FFNN_INPUT]);
	free(ffnn->weights[FFNN_HIDDEN]);
	free(ffnn->nodes[FFNN_INPUT]);
        free(ffnn->nodes[FFNN_HIDDEN]);
        free(ffnn->output_errors);
        free(ffnn->hidden_errors);

        free(ffnn);
}

static void ffnn_set_weights(ffnn_t *ffnn, uint32_t layer, uint32_t node, const double *weights)
{
	double *dst;
        assert(layer < FFNN_OUTPUT);

        dst = &FFNN_WEIGHT(ffnn, layer, node, 0);
        memcpy(dst, weights, sizeof(double) * ffnn->nodes_count[layer]);
}

void ffnn_random_weights(ffnn_t *ffnn)
{
	uint32_t node_src, node_dst;

	for (node_src = 0; node_src < ffnn->nodes_count[FFNN_INPUT]; node_src++)
	{
		for (node_dst = 0; node_dst < ffnn->nodes_count[FFNN_HIDDEN]; node_dst++)
		{
			FFNN_WEIGHT(ffnn, FFNN_INPUT, node_src, node_dst) =
				gaussrand(0.0, powl(ffnn->nodes_count[FFNN_INPUT], -0.5));
		}
	}

	for (node_src = 0; node_src < ffnn->nodes_count[FFNN_HIDDEN]; node_src++)
	{
		for (node_dst = 0; node_dst < ffnn->nodes_count[FFNN_OUTPUT]; node_dst++)
		{
			FFNN_WEIGHT(ffnn, FFNN_HIDDEN, node_src, node_dst) =
				gaussrand(0.0, powl(ffnn->nodes_count[FFNN_HIDDEN], -0.5));
		}
	}
}

static double ffnn_sigmoid(double signal)
{
	return 1.0 / (1.0 + exp(-signal));
}

/* Compute the signal that arrives on a node
 */
double ffnn_node_incoming_signal(ffnn_t *ffnn, const double *input, uint32_t layer, uint32_t node)
{
	uint32_t node_src;
	double sum = 0.0;

	for (node_src = 0; node_src < ffnn->nodes_count[layer]; node_src++)
		sum += input[node_src] * FFNN_WEIGHT(ffnn, layer, node_src, node);

	return sum;
}

const double *ffnn_query(ffnn_t *ffnn, const double *in_signal)
{
	uint32_t node_dst;
	double sum;

	/* update nodes at hidden layer
	 */
	for (node_dst = 0; node_dst < ffnn->nodes_count[FFNN_HIDDEN]; node_dst++)
	{
		sum = ffnn_node_incoming_signal(ffnn,
		                                in_signal,
		                                FFNN_INPUT,
		                                node_dst);
		FFNN_NODE(ffnn, FFNN_HIDDEN, node_dst) = ffnn_sigmoid(sum);
	}

	/* update nodes at output layer
	 */
	for (node_dst = 0; node_dst < ffnn->nodes_count[FFNN_OUTPUT]; node_dst++)
	{
		sum = ffnn_node_incoming_signal(ffnn,
		                                &FFNN_NODE(ffnn, FFNN_HIDDEN, 0),
		                                FFNN_HIDDEN,
		                                node_dst);
		FFNN_NODE(ffnn, FFNN_OUTPUT, node_dst) = ffnn_sigmoid(sum);
	}

	return &FFNN_NODE(ffnn, FFNN_OUTPUT, 0);
}

void ffnn_train(ffnn_t *ffnn, const double *in_signal, const double *target_signal)
{
	uint32_t node_src, node_dst;
	double sum, error;

	ffnn_query(ffnn, in_signal);

	/* Compute errors at output layer
	 */
	for (node_src = 0; node_src < ffnn->nodes_count[FFNN_OUTPUT]; node_src++)
	{
		ffnn->output_errors[node_src] =
			target_signal[node_src] - FFNN_NODE(ffnn, FFNN_OUTPUT, node_src);
	}

	/* Compute errors at hidden layer
	 */
	for (node_src = 0; node_src < ffnn->nodes_count[FFNN_HIDDEN]; node_src++)
	{
		error = 0.0;
		for (node_dst = 0; node_dst < ffnn->nodes_count[FFNN_OUTPUT]; node_dst++)
		{
			error += FFNN_WEIGHT(ffnn, FFNN_HIDDEN, node_src, node_dst) *
				ffnn->output_errors[node_dst];
		}
		ffnn->hidden_errors[node_src] = error;
	}

	/* backpropagate errors at hidden layer
	 */
	for (node_dst = 0; node_dst < ffnn->nodes_count[FFNN_OUTPUT]; node_dst++)
	{
		sum = FFNN_NODE(ffnn, FFNN_OUTPUT, node_dst);
		for (node_src = 0; node_src < ffnn->nodes_count[FFNN_HIDDEN]; node_src++)
		{
			FFNN_WEIGHT(ffnn, FFNN_HIDDEN, node_src, node_dst) +=
				ffnn->output_errors[node_dst] * ffnn->learning_rate * sum * (1.0 - sum) *
				FFNN_NODE(ffnn, FFNN_HIDDEN, node_src);
		}
	}

	/* backpropagate errors at input layer
	 */
	for (node_dst = 0; node_dst < ffnn->nodes_count[FFNN_HIDDEN]; node_dst++)
	{
		sum = FFNN_NODE(ffnn, FFNN_HIDDEN, node_dst);
		for (node_src = 0; node_src < ffnn->nodes_count[FFNN_INPUT]; node_src++)
		{
			FFNN_WEIGHT(ffnn, FFNN_INPUT, node_src, node_dst) +=
				ffnn->hidden_errors[node_dst] * ffnn->learning_rate *
				sum * (1.0 - sum) * in_signal[node_src];
		}
	}
}

int ffnn_autotest(void)
{
        int ret;
        ffnn_t *ffnn;
        const double *output, *w;

        ret = ffnn_new(&ffnn, 3, 3, 3, 0.3);
        if (ret != FFNN_OK)
                return ret;

        /* input -> hidden weights
         */
        ffnn_set_weights(ffnn, FFNN_INPUT, 0, (double []){-1.11963345, -0.08942713, -1.14987363});
        ffnn_set_weights(ffnn, FFNN_INPUT, 1, (double []){-0.0490609,  1.04172926, 0.79359118});
        ffnn_set_weights(ffnn, FFNN_INPUT, 2, (double []){-0.11985223, -0.48123993, -0.19490802});

        /* hidden -> output weights
         */
        ffnn_set_weights(ffnn, FFNN_HIDDEN, 0, (double []){-0.26432754, 0.09763022, -1.17080318});
        ffnn_set_weights(ffnn, FFNN_HIDDEN, 1, (double []){0.25406547, 0.07461128, 0.07121305});
        ffnn_set_weights(ffnn, FFNN_HIDDEN, 2, (double []){0.24099774, -1.14218538, -0.3400072});

        output = ffnn_query(ffnn, (double []){1.0, 0.5, -1.5});
        if (memcmp(output, (double[]){0.553144872362901, 0.41140588894088947, 0.40118425964515297},
                   sizeof(double) * 3))
        {
	        assert(0);
	        return FFNN_EINVAL;
        }

        ffnn_train(ffnn,
                   (double []){1.0, 0.5, -1.5},
                   (double []){1.4, 0.8, 0.4});

        w = &FFNN_WEIGHT(ffnn, FFNN_INPUT, 0, 0);
        if (memcmp(w, (double[]){-1.1306950560190352, -0.076076139881577257, -1.1669052753846141},
                   sizeof(double) * 3))
        {
	        assert(0);
	        return FFNN_EINVAL;
        }

        w = &FFNN_WEIGHT(ffnn, FFNN_INPUT, 1, 0);
        if (memcmp(w, (double[]){-0.054591703009517628, 1.0484047550592115, 0.78507535730769296},
                   sizeof(double) * 3))
        {
	        assert(0);
	        return FFNN_EINVAL;
        }

        w = &FFNN_WEIGHT(ffnn, FFNN_INPUT, 2, 0);
        if (memcmp(w, (double[]){-0.10325982097144711,-0.50126641517763404, -0.16936055192307911},
                   sizeof(double) * 3))
        {
	        assert(0);
	        return FFNN_EINVAL;
        }

        w = &FFNN_WEIGHT(ffnn, FFNN_HIDDEN, 0, 0);
        if (memcmp(w, (double[]){-0.24699562070257053, 0.10542160260532871, -1.1708267367798573},
                   sizeof(double) * 3))
        {
	        assert(0);
	        return FFNN_EINVAL;
        }

        w = &FFNN_WEIGHT(ffnn, FFNN_HIDDEN, 1, 0);
        if (memcmp(w, (double[]){0.30179794926802767,0.096068915608381836, 0.071148174120597499},
                   sizeof(double) * 3))
        {
	        assert(0);
	        return FFNN_EINVAL;
        }

        w = &FFNN_WEIGHT(ffnn, FFNN_HIDDEN, 2, 0);
        if (memcmp(w, (double[]){0.26528846180739413, -1.1312657406852082, -0.34004021487713904},
                   sizeof(double) * 3))
        {
	        assert(0);
	        return FFNN_EINVAL;
        }

        return FFNN_OK;
}
